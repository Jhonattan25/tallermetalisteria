package Reportes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public interface Reporte {

	 public  void generarReporte(String DES, String titulo,String header[],String body[],float[] hanchoColum) throws SQLException, FileNotFoundException, IOException;

}