package Reportes;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.StringTokenizer;

@SuppressWarnings("deprecation")
public class ReportePDF implements Reporte {

	@Override
	public void generarReporte(String DES, String titulo, String header[], String body[], float[] anchoColum)
			throws SQLException, FileNotFoundException, IOException {
		// se define el documento a escribir y se le agregga el nombre
		PdfWriter write = new PdfWriter(DES);
		// se genera el documento pdf
		PdfDocument pdf = new PdfDocument(write);
		//tama�o de letra de la pagina  y se da formato horizontal
		Document document = new Document(pdf, PageSize.A4.rotate());
		//se le agrega el titulo
		EventoPDF evento = new EventoPDF(document, titulo);
		
		pdf.addEventHandler(PdfDocumentEvent.END_PAGE, evento);
		document.setMargins(75, 36, 75, 36);
		PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
		PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

		Table table = new Table(anchoColum);
		table.setWidth(UnitValue.createPercentValue(100));

		for (String Columna : header) {
			process(table, Columna, bold, true);
		}
		for (String fila : body) {
			process(table, fila, font, false);
		}
		document.add(table);
		document.close();

	}

	private void process(Table table, String lineData, PdfFont bold, boolean flag) {
		StringTokenizer tokenizer = new StringTokenizer(lineData, ";");
		while (tokenizer.hasMoreTokens()) {
			if (flag) {
				table.addHeaderCell(new Cell().add(new Paragraph(tokenizer.nextToken()).setFont(bold)));
			} else {
				table.addCell(new Cell().add(new Paragraph(tokenizer.nextToken()).setFont(bold)));
			}

		}

	}

}
