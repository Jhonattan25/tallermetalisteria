package tallerMetalisteria.controlador;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import tallerMetalisteria.modelo.Empleado;
import tallerMetalisteria.persistencia.EmpleadoDAO;

public class IniciarSesionController implements Initializable {
	
	@FXML
	private TextField txtCedula;
	@FXML
	private PasswordField txtContrasenia;
	@FXML
	private Button btnIngresar;
	@FXML
	private Button btnCerrar;

	@FXML
	public void validarDatos(ActionEvent e) throws IOException {
		String cedula = txtCedula.getText();
		String contrasenia = txtContrasenia.getText();

		if (!cedula.isEmpty() && !contrasenia.isEmpty() ) {
			
			try {
				Empleado empleado = EmpleadoDAO.verificarLogin(Integer.parseInt(cedula), contrasenia);
				
				if(empleado == null)
					throw new Exception();
				
				Stage stage2 = new Stage();
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/VentanaPrincipal.fxml"));
				
				Parent p = loader.load();
				Scene s = new Scene(p);
				
				PrincipalController empleadoController = (PrincipalController) loader.getController();
				empleadoController.recibirParametros(empleado);
				
				stage2.setScene(s);
				stage2.show();
				System.out.println("Empleado: " + empleado.getNombreCompleto());
							
				cerrarVentana();
		
			} catch (Exception e1) {
				Alert a21 = new Alert(AlertType.WARNING);
				a21.setTitle("ADVERTENCIA");
				a21.setHeaderText(null);
				a21.setContentText("El Email o el Password son incorrectos");
				
				a21.show();
			}
		} else {
			Alert a = new Alert(AlertType.WARNING);
			a.setTitle("ADVERTENCIA");
			a.setHeaderText(null);
			a.setContentText("Por favor llene los campos de texto");
			
			a.show();
		}
	}
	
	@FXML
	public void recuperarPassword(ActionEvent e) {
		
		
//		TextInputDialog email = new TextInputDialog();
//		email.setTitle("Recuperar Contrase�a");
//		email.setHeaderText(null);
//		email.setContentText("Ingrese Email");
//		
//		
//		java.util.Optional<String> resultado = email.showAndWait();
//		
//		if (!resultado.get().isEmpty())
//		{
//			if(!delegado.verificarCorreo(resultado.get()).isEmpty())
//			{
//				try 
//				{
//					delegado.enviarEmail(resultado.get());
//				} catch (Exception e1) 
//				{
//					Alert a = new Alert(AlertType.ERROR);
//					a.setTitle("ERROR");
//					a.setHeaderText(null);
//					a.setContentText("Error al enviar el correo");
//					
//					a.show();
//				}
//			}
//			else
//			{
//				Alert a = new Alert(AlertType.ERROR);
//				a.setTitle("ERROR");
//				a.setHeaderText(null);
//				a.setContentText("No existe un administrador con este correo");
//				
//				a.show();
//			}
//		}else if(resultado.get().equals(""))
//		{
//			Alert a = new Alert(AlertType.WARNING);
//			a.setTitle("ADVERTENCIA");
//			a.setHeaderText(null);
//			a.setContentText("Digite su email");
//			
//			a.show();
//		}        
	}
	
	@FXML
	public void cerrarVentana() {
		Stage stageCerrar = (Stage) btnIngresar.getScene().getWindow();
		stageCerrar.hide();
	}
	
	@FXML
	public void cerrar (ActionEvent e) {
		Stage stageCerrar = (Stage) btnCerrar.getScene().getWindow();
		stageCerrar.close();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}
}
