package tallerMetalisteria.controlador;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Reportes.Reporte;
import Reportes.ReportePDF;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tallerMetalisteria.modelo.Local;
import tallerMetalisteria.persistencia.LocalDAO;

public class SReporteComplejo implements Initializable{

	@FXML
	private TableView<Local> tablaLocal;
	@FXML
	private TableColumn<Local, String> colIdLocal;
	@FXML
	private TableColumn<Local, String> colNEmpleados;
	
	ObservableList<Local> listaObservable = FXCollections.observableArrayList();
	private LocalDAO localPersistencia = new LocalDAO();

	public void consultar() {
		
		listaObservable.clear();
		this.colIdLocal.setCellValueFactory(new PropertyValueFactory<Local, String>("codigo"));
		this.colNEmpleados.setCellValueFactory(new PropertyValueFactory<Local, String>("numEmpleados"));
		ArrayList<Local> listaLocales = this.localPersistencia.numeroEmpleadosDB();
		for (Local local : listaLocales) {
			listaObservable.add(local);
			System.out.println(local.toString());
		}
		this.tablaLocal.setItems(listaObservable);
	}
	
	public void generarReporte() {

		Reporte reporte = new ReportePDF();
		String tituloTable[] = { "Codigo", "Numero de empleados" };
		float[] anchoColum = { 3, 2};
		LocalDAO localPersistencia = new LocalDAO();

		ArrayList<Local> listaLocales = localPersistencia.numeroEmpleadosDB();
		String[] dataStrings = new String[listaLocales.size()];
		for (int i = 0; i < listaLocales.size(); i++) {
			dataStrings[i] = listaLocales.get(i).toString();
		}
		try {
			reporte.generarReporte("numEpleadosLocales.pdf", "NUMERO DE LOS EMPLEADOS EN CADA LOCAL", tituloTable, dataStrings,anchoColum);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}

}
