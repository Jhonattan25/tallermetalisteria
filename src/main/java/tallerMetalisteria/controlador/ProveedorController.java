package tallerMetalisteria.controlador;

import tallerMetalisteria.modelo.*;
import tallerMetalisteria.persistencia.CiudadDAO;
import tallerMetalisteria.persistencia.ProveedorDAO;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;


import Reportes.Reporte;
import Reportes.ReportePDF;


public class ProveedorController implements Initializable {

	@FXML
	private TableView<Proveedor> tablaProveedor;
	@FXML
	private TableColumn<Proveedor, String> colId;
	@FXML
	private TableColumn<Proveedor, String> colNombre;
	@FXML
	private TableColumn<Proveedor, String> colEmail;
	@FXML
	private TableColumn<Proveedor, String> colDireccion;
	@FXML
	private TableColumn<Proveedor, String> colTelefono;
	@FXML
	private TableColumn<Proveedor, String> colCiudad;
	@FXML
	private TextField TextCedula;
	@FXML
	private TextField TextNombre;
	@FXML
	private TextField TextCorreo;
	@FXML
	private TextField TextDireccion;
	@FXML
	private TextField TextTelefono;
	@FXML
	private Button ButtonActualizar;
	@FXML
	private Button BtnEliminar;
	@FXML
	private Label LabelInformacion;
	@FXML
	private ComboBox<Ciudad> comboBoxCiudad;

	private ObservableList<Ciudad> listaComboCiudad;

	private ArrayList<Ciudad> listaCiudadProveedor;

	ObservableList<Proveedor> listaobservable = FXCollections.observableArrayList();
	private ProveedorDAO ProveedorPersistencia = new ProveedorDAO();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		iniciarProveerdo();
		llenarComboCiudad();
	}

	public void iniciarProveerdo() {
		this.colId.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("id"));
		this.colNombre.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("nombre"));
		this.colEmail.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("email"));
		this.colDireccion.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("direccion"));
		this.colTelefono.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("telefono"));
		this.colCiudad.setCellValueFactory(new PropertyValueFactory<Proveedor, String>("ciudad_id"));
		ArrayList<Proveedor> ListaProveedor = this.ProveedorPersistencia.listarProveedoresDB();
		for (Proveedor proveedor : ListaProveedor) {
			listaobservable.add(proveedor);
		}
		this.tablaProveedor.setItems(listaobservable);
	}

	@FXML
	public void Guardar() {
		if (!validarDatos("Por Favor llenar todos los campos.")) {
			this.ProveedorPersistencia.crearProveedorDB(new Proveedor(TextCedula.getText(), TextNombre.getText(),
					TextCorreo.getText(), TextDireccion.getText(), TextTelefono.getText(), comboBoxCiudad.getValue().getId()));
			listaobservable.clear();
			limpiarDatos();
		}
	}

	@FXML
	public void actualizar(ActionEvent e) {
		if (!validarDatos("Por Favor llenar todos los campos.")) {
			this.ProveedorPersistencia.actualizarProveedorDB(new Proveedor(TextCedula.getText(), TextNombre.getText(),
					TextCorreo.getText(), TextDireccion.getText(), TextTelefono.getText(), comboBoxCiudad.getValue().getId()));
			listaobservable.clear();
			limpiarDatos();
		}
	}

	@FXML
	public void Eliminar() {
		this.ProveedorPersistencia.eliminarProveedorDB(Integer.parseInt(TextCedula.getText()));
		listaobservable.clear();
		limpiarDatos();
	}

	@FXML
	public void onEdit(MouseEvent event) {

		if (tablaProveedor.getSelectionModel().getSelectedItem() != null) {
			Proveedor proveedor = tablaProveedor.getSelectionModel().getSelectedItem();
			TextCedula.setText(proveedor.getId());
			TextNombre.setText(proveedor.getNombre());
			TextCorreo.setText(proveedor.getEmail());
			TextDireccion.setText(proveedor.getDireccion());
			TextTelefono.setText(proveedor.getTelefono());
		}
	}

	public void limpiarDatos() {
		TextCedula.setText("");
		TextNombre.setText("");
		TextCorreo.setText("");
		TextDireccion.setText("");
		TextTelefono.setText("");
		iniciarProveerdo();
	}

	public void generarReporte() {

		Reporte reporte = new ReportePDF();
		String tituloTable[] = { "Cedula", "Nombre", "Correo", "Direccion", "Telefono", "Ciudad" };
		float[] anchoColum = { 3, 3, 2, 3, 2, 3 };
		ProveedorDAO ProveedorPersistencia = new ProveedorDAO();

		ArrayList<Proveedor> ListaProveedor = ProveedorPersistencia.listarProveedoresDB();
		String[] dataStrings = new String[ListaProveedor.size()];
		for (int i = 0; i < ListaProveedor.size(); i++) {
			dataStrings[i] = ListaProveedor.get(i).toString();
		}
		try {
			reporte.generarReporte("proveedores.pdf", "PROVEEDORES", tituloTable, dataStrings,anchoColum);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public boolean validarDatos(String mensaje) {
		if (TextCedula.getText().isEmpty() || TextNombre.getText().isEmpty() || TextCorreo.getText().isEmpty()
				|| TextCorreo.getText().isEmpty() || TextTelefono.getText().isEmpty()) {
			Alert a = new Alert(AlertType.WARNING);
			a.setTitle("Advertencia");
			a.setHeaderText(null);
			a.setContentText(mensaje);
			a.show();
			return true;
		} else {
			return false;
		}
	}
	
	public void llenarComboCiudad() {
		listaCiudadProveedor = CiudadDAO.listarCiudadesDB();
		listaComboCiudad = FXCollections.observableArrayList(listaCiudadProveedor);
		comboBoxCiudad.setItems(listaComboCiudad);
	}
	
}
