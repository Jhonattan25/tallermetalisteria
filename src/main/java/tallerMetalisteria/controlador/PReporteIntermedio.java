package tallerMetalisteria.controlador;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Reportes.Reporte;
import Reportes.ReportePDF;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import tallerMetalisteria.modelo.Local;
import tallerMetalisteria.persistencia.LocalDAO;

public class PReporteIntermedio implements Initializable{

	@FXML
	private TableView<Local> tablaLocal;
	@FXML
	private TableColumn<Local, String> colIdLocal;
	@FXML
	private TableColumn<Local, String> colNombreLocal;
	@FXML
	private TableColumn<Local, String> colNumVentas;
	
	ObservableList<Local> listaObservable = FXCollections.observableArrayList();
	private LocalDAO localPersistencia = new LocalDAO();

	public void consultar() {
		
		listaObservable.clear();
		this.colIdLocal.setCellValueFactory(new PropertyValueFactory<Local, String>("codigo"));
		this.colNombreLocal.setCellValueFactory(new PropertyValueFactory<Local, String>("nombre"));
		this.colNumVentas.setCellValueFactory(new PropertyValueFactory<Local, String>("numVentas"));
		ArrayList<Local> listaLocales = this.localPersistencia.numeroVentasDB();
		for (Local local : listaLocales) {
			listaObservable.add(local);
		}
		this.tablaLocal.setItems(listaObservable);
	}
	
	public void generarReporte() {

		Reporte reporte = new ReportePDF();
		String tituloTable[] = { "Codigo", "Nombre","Numero de ventas" };
		float[] anchoColum = { 3, 3, 2};
		LocalDAO localPersistencia = new LocalDAO();

		ArrayList<Local> listaLocales = localPersistencia.numeroVentasDB();
		String[] dataStrings = new String[listaLocales.size()];
		for (int i = 0; i < listaLocales.size(); i++) {
			dataStrings[i] = listaLocales.get(i).toStringVentas();
		}
		try {
			reporte.generarReporte("numVentas.pdf", "NUMERO DE LAS VENTAS DE CADA LOCAL", tituloTable, dataStrings, anchoColum);
			
			Alert a = new Alert(AlertType.INFORMATION);
			// set alert type
			a.setTitle("Información");
			a.setHeaderText(null);

			// set content text
			a.setContentText("Se ha realizado el reporte");

			// show the dialog
			a.show();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
