package tallerMetalisteria.controlador;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Reportes.Reporte;
import Reportes.ReportePDF;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import tallerMetalisteria.modelo.Local;
import tallerMetalisteria.modelo.Producto;
import tallerMetalisteria.persistencia.LocalDAO;
import tallerMetalisteria.persistencia.ProductoDAO;

public class SReporteIntermedio implements Initializable{
	
	@FXML
	private TableView<Producto> tablaProducto;
	@FXML
	private TableColumn<Producto, String> colCodigo;
	@FXML
	private TableColumn<Producto, String> colNombre;
	@FXML
	private TableColumn<Producto, String> colDescripcion;
	@FXML
	private TableColumn<Producto, String> colIva;
	@FXML
	private TableColumn<Producto, String> colPrecio;
	
	ObservableList<Producto> listaObservable = FXCollections.observableArrayList();
	private ProductoDAO productoPersistencia = new ProductoDAO();

	public void consultar() {
		
		listaObservable.clear();
		this.colCodigo.setCellValueFactory(new PropertyValueFactory<Producto, String>("codigo"));
		this.colNombre.setCellValueFactory(new PropertyValueFactory<Producto, String>("nombre"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<Producto, String>("descripcion"));
		this.colIva.setCellValueFactory(new PropertyValueFactory<Producto, String>("iva"));
		this.colPrecio.setCellValueFactory(new PropertyValueFactory<Producto, String>("precio"));
		ArrayList<Producto> listaProductos = this.productoPersistencia.productosFechaDB();
		for (Producto producto : listaProductos) {
			listaObservable.add(producto);
		}
		this.tablaProducto.setItems(listaObservable);
	}
	
	public void generarReporte() {

		Reporte reporte = new ReportePDF();
		String tituloTable[] = { "Codigo", "Nombre", "Descripcion", "Iva", "Precio" };
		float[] anchoColum = { 3, 3, 5, 2, 3};
		ProductoDAO productoPersistencia = new ProductoDAO();

		ArrayList<Producto> listaProductos = productoPersistencia.productosFechaDB();
		String[] dataStrings = new String[listaProductos.size()];
		for (int i = 0; i < listaProductos.size(); i++) {
			dataStrings[i] = listaProductos.get(i).toString();
		}
		try {
			reporte.generarReporte("productosFecha.pdf", "PRODUCTOS VENDIDOS ENTRE 2020/12/01 Y 2021/02/01", tituloTable, dataStrings,anchoColum);
			
			Alert a = new Alert(AlertType.INFORMATION);
			// set alert type
			a.setTitle("Información");
			a.setHeaderText(null);

			// set content text
			a.setContentText("Se ha realizado el reporte");

			// show the dialog
			a.show();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
