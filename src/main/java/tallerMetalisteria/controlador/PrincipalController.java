package tallerMetalisteria.controlador;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import tallerMetalisteria.modelo.Empleado;

public class PrincipalController implements Initializable {

	@FXML
	private Label labelNombreEmpleado;
	
	@FXML
	private Button btnCerrarSesion;

	
	public void recibirParametros(Empleado empleado) {
		
		labelNombreEmpleado.setText("Bienvenido " + empleado.getNombreCompleto());
	}

	@FXML
	public void crudCliente(ActionEvent e) throws IOException
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/cliente.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}

	@FXML
	public void crudProveedor(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/vistaProveedor.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void reporteSimple(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/VentanaReporteSimple.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void primerIntermedio(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/VentanaPReporteIntermedio.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void segundoIntermedio(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/VentanaSReporteIntermedio.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void primerComplejo(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/VentanaPReporteComplejo.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void segundoComplejo(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/VentanaSReporteComplejo.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void tercerComplejo(ActionEvent e) throws IOException 
	{
		Stage stage = new Stage();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/VentanaTReporteComplejo.fxml"));

		Parent p = loader.load();
		Scene s = new Scene(p);

		stage.setScene(s);
		stage.show();
	}
	
	@FXML
	public void cerrarSesion (ActionEvent e) throws IOException 
	{
		Stage stageCerrar = (Stage) btnCerrarSesion.getScene().getWindow();
		stageCerrar.hide();
		
		Stage primaryStage = new Stage();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/iniciarSesion.fxml"));
		
		Parent p = loader.load();
		Scene s = new Scene(p);
		
		primaryStage.setTitle("Iniciar Sesion");
		primaryStage.setResizable(false);
		primaryStage.setScene(s);
		primaryStage.show();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
}
