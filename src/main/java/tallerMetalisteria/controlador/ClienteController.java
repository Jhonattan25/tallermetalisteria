package tallerMetalisteria.controlador;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.text.TabableView;

import Reportes.Reporte;
import Reportes.ReportePDF;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import tallerMetalisteria.modelo.Ciudad;
import tallerMetalisteria.modelo.Cliente;
import tallerMetalisteria.modelo.Proveedor;
import tallerMetalisteria.persistencia.CiudadDAO;
import tallerMetalisteria.persistencia.ClienteDAO;
import tallerMetalisteria.persistencia.ProveedorDAO;

public class ClienteController implements Initializable {

	@FXML
	private TextField textDocumento;
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textEmail;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textDireccion;
	@FXML
	private TextField textGenero;
	@FXML
	private ComboBox<Ciudad> comboBoxCiudad;
	@FXML
	private Button btnRegistrarCliente;
	@FXML
	private Button btnActualizarCliente;
	@FXML
	private Button  btnEliminarCliente;
	
	private ObservableList<Ciudad> listaComboCiudad;

	private ArrayList<Ciudad> listaCiudadCliente;

	// Table

	@FXML
	private TableView<Cliente> tablaCliente;
	@FXML
	private TableColumn<Cliente, String> columDocumento;
	@FXML
	private TableColumn<Cliente, String> columnNombre;
	@FXML
	private TableColumn<Cliente, String> columEmail;
	@FXML
	private TableColumn<Cliente, String> columnTel;
	@FXML
	private TableColumn<Cliente, String> columDireccion;
	@FXML
	private TableColumn<Cliente, String> columGenero;
	@FXML
	private TableColumn<Cliente, String> columCiudad;

	ObservableList<Cliente> listaClientes = FXCollections.observableArrayList();
	
	private ClienteDAO clientePersistencia = new ClienteDAO();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		llenarComboCiudad();
		listarClientes();
		
		
	}

	@FXML
	private void handleButtonAction(ActionEvent e) {
		
	}

	public void registrarCliente() {
		
		String nombre = textNombre.getText();
		String email = textEmail.getText();
		String telefono = textTelefono.getText();
		String direccion = textDireccion.getText();
		String genero = textGenero.getText();
		

		try {
			if (!textDocumento.getText().isEmpty() && !nombre.isEmpty() && !email.isEmpty() 
					&& !telefono.isEmpty() && !direccion.isEmpty() && !genero.isEmpty()
					&& !comboBoxCiudad.getId().isEmpty()) {
				
				int documento = Integer.parseInt(textDocumento.getText());
				int ciudad = comboBoxCiudad.getValue().getId();
				Cliente cliente = new Cliente();
				cliente.setDocumento(documento);
				cliente.setNombre(nombre);
				cliente.setEmail(email);
				cliente.setTelefono(telefono);
				cliente.setDireccion(direccion);
				cliente.setGenero(genero);
				cliente.setCiudad_id(ciudad);

				clientePersistencia.crearClienteDB(cliente);
				listaClientes.clear();
				listarClientes();
				limpiarCampos();

			} else {
				Alert a = new Alert(AlertType.INFORMATION);
				// set alert type
				a.setTitle("Información");
				a.setHeaderText(null);

				// set content text
				a.setContentText("Llenar todo los datos");

				// show the dialog
				a.show();
				System.out.println("No se guardo el cliente");
			}
		} catch (Exception e) {
			Alert a = new Alert(AlertType.INFORMATION);
			// set alert type
			a.setTitle("Información");
			a.setHeaderText(null);

			// set content text
			a.setContentText("Llenar todo los datos");

			// show the dialog
			a.show();
		}
	}

	public void listarClientes() {

		this.columDocumento.setCellValueFactory(new PropertyValueFactory<Cliente, String>("documento"));
		this.columnNombre.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nombre"));
		this.columEmail.setCellValueFactory(new PropertyValueFactory<Cliente, String>("email"));
		this.columnTel.setCellValueFactory(new PropertyValueFactory<Cliente, String>("telefono"));
		this.columDireccion.setCellValueFactory(new PropertyValueFactory<Cliente, String>("direccion"));
		this.columGenero.setCellValueFactory(new PropertyValueFactory<Cliente, String>("genero"));
		this.columCiudad.setCellValueFactory(new PropertyValueFactory<Cliente, String>("ciudad_id"));

		ArrayList<Cliente> listaArrayClientes = clientePersistencia.listarClienteDB();

		for (Cliente clientes : listaArrayClientes) {
			listaClientes.add(clientes);
		}

		this.tablaCliente.setItems(listaClientes);
	}

	public void actualizarCliente() {
		if (!textDocumento.getText().isEmpty() && !textNombre.getText().isEmpty() 
				&& !textEmail.getText().isEmpty() && !textTelefono.getText().isEmpty() 
				&& !textDireccion.getText().isEmpty() && !textGenero.getText().isEmpty()
				&& !comboBoxCiudad.getId().isEmpty()) {
			
			Cliente clienteActualizar = new Cliente(Integer.parseInt(textDocumento.getText()),
					textNombre.getText(), textEmail.getText(), textTelefono.getText(),
					textDireccion.getText(), textGenero.getText(), Integer.parseInt(comboBoxCiudad.getId()));
			
			clientePersistencia.actualizarClienteDB(clienteActualizar);
			
			listaClientes.clear();
			listarClientes();
			limpiarCampos();
			
		}else {
			Alert a = new Alert(AlertType.INFORMATION);
			// set alert type
			a.setTitle("Información");
			a.setHeaderText(null);

			// set content text
			a.setContentText("Llenar todo los datos");

			// show the dialog
			a.show();
		}
		
	}
	
	public void eliminarCliente() {		
		try {
			
			if (!textDocumento.getText().isEmpty()) {
				int documento = Integer.parseInt(textDocumento.getText());
				clientePersistencia.eliminarClienteDB(documento);
				listaClientes.clear();
				limpiarCampos();
				listarClientes();
			}else {
				Alert a = new Alert(AlertType.INFORMATION);
				// set alert type
				a.setTitle("Información");
				a.setHeaderText(null);

				// set content text
				a.setContentText("Llenar todo los datos");

				// show the dialog
				a.show();
			}
			
			
		} catch (Exception e) {
			System.out.println("Hola");
		}
	}
	

	public void seleccionarCamposTabla() {
		if (tablaCliente.getSelectionModel().getSelectedItem() != null) {
			Cliente clienteSeleccionado = tablaCliente.getSelectionModel().getSelectedItem();
			textDocumento.setText(clienteSeleccionado.getDocumento()+"");
			textNombre.setText(clienteSeleccionado.getNombre());
			textEmail.setText(clienteSeleccionado.getEmail());
			textTelefono.setText(clienteSeleccionado.getTelefono());
			textDireccion.setText(clienteSeleccionado.getDireccion());
			textGenero.setText(clienteSeleccionado.getGenero());
			comboBoxCiudad.setId(clienteSeleccionado.getCiudad_id()+"");
		}
	}

	public void limpiarCampos() {
		textDocumento.setText("");
		textNombre.setText("");
		textEmail.setText("");
		textTelefono.setText("");
		textDireccion.setText("");
		textGenero.setText("");
		comboBoxCiudad.setId("");
	}
	
	
	private void llenarComboCiudad() {
		listaCiudadCliente = CiudadDAO.listarCiudadesDB();
		listaComboCiudad = FXCollections.observableArrayList(listaCiudadCliente);
		comboBoxCiudad.setItems(listaComboCiudad);
	}
	
	public void generarReporte() {

		Reporte reporte = new ReportePDF();
		String tituloTable[] = { "Documento", "Nombre", "Correo", "Telefono", "Direccion", "Genero", "Ciudad" };
		float[] anchoColum = { 2, 3, 3, 2, 3, 1, 1 };
		ClienteDAO clientePersistencia = new ClienteDAO();

		ArrayList<Cliente> listaClientes = clientePersistencia.listarClienteDB();
		String[] dataStrings = new String[listaClientes.size()];
		for (int i = 0; i < listaClientes.size(); i++) {
			dataStrings[i] = listaClientes.get(i).toString();
		}
		try {
			reporte.generarReporte("clientes.pdf", "REPORTE CLIENTES", tituloTable, dataStrings,anchoColum);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

}
