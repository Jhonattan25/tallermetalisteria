package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Fabrica;
import tallerMetalisteria.modelo.Pedido;

public class PedidoDAO {

	public static void crearPedidoDB(Pedido pedido) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `pedido` (`codigo`, `precioTotal`, `cantidadUnidades`, `fechaSolicitud`, `descuento`, `Proveedor_id`, `Fabrica_codigo`) VALUES (?, ?, ?, ?, ?, ?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, pedido.getCodigo());
				ps.setDouble(2, pedido.getPrecioTotal());
				ps.setInt(3, pedido.getCantidadUnidades());
				ps.setDate(4, pedido.getFechaSolicitud());
				ps.setDouble(5, pedido.getDescuento());
				ps.setString(6, pedido.getProveedor_id());
				ps.setString(7, pedido.getFabrica_codigo());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Pedido con codigo " + pedido.getCodigo() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Pedido");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public static ArrayList<Pedido> listarPedidoDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Pedido> pedidos = new ArrayList<Pedido>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `pedido`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				pedidos.add(new Pedido(rs.getString("codigo"), rs.getDouble("precioTotal"), rs.getInt("cantidadUnidades"),
						rs.getDate("fechaSolicitud"), rs.getDouble("descuento"), rs.getString("Proveedor_id"), rs.getString("Fabrica_codigo")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Pedidos");
			System.out.println(e);
		}

		return pedidos;
	}

	public static void actualizarPedidoDB(Pedido pedido) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "UPDATE pedido SET precioTotal = ?, cantidadUnidades = ?, fechaSolicitud = ?, descuento = ?, Proveedor_id = ?, Fabrica_codigo = ? WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setDouble(1, pedido.getPrecioTotal());
				ps.setInt(2, pedido.getCantidadUnidades());
				ps.setDate(3, pedido.getFechaSolicitud());
				ps.setDouble(4, pedido.getDescuento());
				ps.setString(5, pedido.getProveedor_id());
				ps.setString(6, pedido.getFabrica_codigo());
				ps.setString(7, pedido.getCodigo());
				ps.executeUpdate();

				System.out.println("El Pedido con el codigo " + pedido.getCodigo() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el Pedido");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}
	}

	public static void eliminarPedidoDB(String codigoPedido) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "DELETE FROM pedido WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, codigoPedido);
				ps.executeUpdate();

				System.out.println("El Pedido con ID " + codigoPedido + " ha sido eliminado");
			} catch (SQLException ex) {
				System.out.println("No se pudo eliminar el Pedido");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}

	}
}
