package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.FondoPensiones;


public class FondoPensionesDAO {
	
	public static void crearFondoPensionesDB(FondoPensiones fonPensiones) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `fondopensiones` (`nombre`, `fechaIngreso`, `estado`)  VALUES (?, ?, ?) ";
				ps = conexion.prepareStatement(query);
				ps.setString(1, fonPensiones.getNombre());
				ps.setDate(2, fonPensiones.getFechaIngreso());
				ps.setBoolean(3, fonPensiones.isEstado());
				
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego fondo de pensiones " + fonPensiones.getNombre() + fonPensiones.getFechaIngreso() + fonPensiones.isEstado() + " con exito");

			} catch (SQLException ex) {
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<FondoPensiones> listarFondoPensionesDB() {
		Conexion db_connect = new Conexion();
		ArrayList<FondoPensiones> fondoPensiones = new ArrayList<FondoPensiones>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `fondopensiones`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				fondoPensiones.add(new FondoPensiones(rs.getInt("id"), rs.getString("nombre"), rs.getDate("fechaIngreso"), rs.getBoolean("estado")));
			}
			
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los EPS");
			System.out.println(e);
		}
		
		return fondoPensiones;
	}
	
	public static void actualizarFondoPensionesDB(FondoPensiones fonPensiones) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE fondopensiones SET nombre = ?, fechaIngreso = ?, estado = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, fonPensiones.getNombre());
				ps.setDate(2, fonPensiones.getFechaIngreso());
				ps.setBoolean(3, fonPensiones.isEstado());
				ps.setInt(4, fonPensiones.getId());
				
				ps.executeUpdate();
				
				System.out.println("fondo de pensiones  " + fonPensiones.getNombre() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar la fondo de pensiones");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarFondoPensionesB(int idFP) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM fondopensiones WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idFP);
				ps.executeUpdate();
				
				System.out.println("La fondo de pensiones con ID " + idFP +" ha sido eliminada");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la fondo de pensiones");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
}
