package tallerMetalisteria.persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Scanner;

import tallerMetalisteria.modelo.ARL;
import tallerMetalisteria.modelo.Cargo;
import tallerMetalisteria.modelo.EPS;
import tallerMetalisteria.modelo.FondoPensiones;
import tallerMetalisteria.modelo.Local;
import tallerMetalisteria.modelo.Categoria;
import tallerMetalisteria.modelo.Ciudad;
import tallerMetalisteria.modelo.Cliente;
import tallerMetalisteria.modelo.Departamento;
import tallerMetalisteria.modelo.Fabrica;
import tallerMetalisteria.modelo.Pais;
import tallerMetalisteria.modelo.Pedido;
import tallerMetalisteria.modelo.Producto;
import tallerMetalisteria.modelo.Proveedor;
import tallerMetalisteria.modelo.Venta;

public class AppService {

	public static void crearPais() {

		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese el nombre del Pa�s");
		String nombrePais = sc.nextLine();

		Pais pais = new Pais(nombrePais);

		PaisDAO.crearPaisDB(pais);
	}

	public static void listarPaises() {
		ArrayList<Pais> paises = PaisDAO.listarPaisesDB();

		for (Pais pais : paises) {
			System.out.println("ID: " + pais.getId());
			System.out.println("Nombre: " + pais.getNombre());
			System.out.println("----------------------------");
		}
	}

	public static void actualizarPais() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingresa el nuevo nombre del Pa�s");

		System.out.println("Indica el ID del Pa�s a editar");
		
		int idPais = sc.nextInt();
		
		System.out.println("Ingrese el nuevo nombre del Pa�s");
		String nombrePais = sc.nextLine();
		
		Pais pais = new Pais(idPais, nombrePais);

		PaisDAO.actualizarPaisDB(pais);
	}

	public static void eliminarPais() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese el ID del Pa�s que desea eliminar");
		int idPais = sc.nextInt();
		PaisDAO.eliminarPaisDB(idPais);
	}
}
