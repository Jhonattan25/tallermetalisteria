package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Cliente;
import tallerMetalisteria.modelo.Empleado;
import tallerMetalisteria.modelo.EmpleadoCargo;

public class EmpleadoDAO {

	public static Empleado verificarLogin(int cedula, String contrasenia) {
		Conexion db_connect = new Conexion();
		Empleado empleado = null;

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM empleado WHERE cedula = ? AND contrasenia = ?";
			ps = conexion.prepareStatement(query);

			ps.setInt(1, cedula);
			ps.setString(2, contrasenia);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			if(rs.next()) {
				empleado = new Empleado(rs.getInt("cedula"), rs.getString("nombreCompleto"), rs.getString("telefono"),
						rs.getString("estadoCivil"), rs.getDate("fechaNacimiento"), rs.getString("direccion"),
						rs.getDate("fechaIngreso"), rs.getString("Fabrica_codigo"), rs.getString("genero"),
						rs.getString("contrasenia"), rs.getString("Local_codigo"), rs.getInt("Ciudad_id"), rs.getInt("EPS_id"),
						rs.getInt("ARL_id"), rs.getInt("FondoPensiones_id"));
			}
			
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Paises");
			System.out.println(e);
		}

		return empleado;
	}
}
