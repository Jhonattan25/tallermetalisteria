package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Cargo;
import tallerMetalisteria.modelo.Pais;

public class CargoDAO {

	public static void crearCargoDB(Cargo cargo) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `cargo` (`id`, `nombre`, `horario`, `salario`, `descripcion`) VALUES (?,?,?,?,?)";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, cargo.getId());
				ps.setString(2, cargo.getNombre());
				ps.setString(3, cargo.getHorario());
				ps.setDouble(4, cargo.getSalario());
				ps.setString(5, cargo.getDescripcion());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Cargo " + cargo.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Cargo");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public static ArrayList<Cargo> listarCargoDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Cargo> cargos = new ArrayList<Cargo>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `cargo`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				cargos.add(new Cargo(rs.getInt("id"), rs.getString("nombre"), rs.getString("horario"), rs.getDouble("salario"),
						rs.getString("descripcion")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Paises");
			System.out.println(e);
		}

		return cargos;
	}

	public static void actualizarCargoDB(Cargo cargo) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "UPDATE cargo SET nombre = ?, horario = ?, salario = ?, descripcion = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, cargo.getNombre());
				ps.setString(2, cargo.getHorario());
				ps.setDouble(3, cargo.getSalario());
				ps.setString(4, cargo.getDescripcion());
				ps.setInt(5, cargo.getId());
				ps.executeUpdate();

				System.out.println("El Cargo " + cargo.getNombre() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el pa�s");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}
	}

	public static void eliminarCargoDB(int idCargo) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "DELETE FROM cargo WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idCargo);
				ps.executeUpdate();

				System.out.println("El cargo con ID " + idCargo + " ha sido eliminado");
			} catch (SQLException ex) {
				System.out.println("No se pudo eliminar el Cargo");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}

	}

}
