package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Departamento;
import tallerMetalisteria.modelo.Pais;

public class DepartamentoDAO {
	
	public static void crearDepartamentoDB(Departamento departamento) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `departamento` (`nombre`, `Pais_id`) VALUES (?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, departamento.getNombre());
				ps.setInt(2, departamento.getPais_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Departamento " + departamento.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Departamento");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<Departamento> listarDepartamentosDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Departamento> departamentos = new ArrayList<Departamento>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `departamento`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				departamentos.add(new Departamento(rs.getInt("id"), rs.getString("nombre"), rs.getInt("Pais_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Departamentos");
			System.out.println(e);
		}
		
		return departamentos;
	}
	
	public static void actualizarDepartamentoDB(Departamento departamento) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE departamento SET nombre = ?, Pais_id = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, departamento.getNombre());
				ps.setInt(2, departamento.getPais_id());
				ps.setInt(3, departamento.getId());
				ps.executeUpdate();
				
				System.out.println("El Departamento " + departamento.getNombre() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el Departamento");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarDepartamentoDB(int idDepartamento) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM departamento WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idDepartamento);
				ps.executeUpdate();
				
				System.out.println("El Departamento con ID " + idDepartamento +" ha sido eliminado");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar el Departamento");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
