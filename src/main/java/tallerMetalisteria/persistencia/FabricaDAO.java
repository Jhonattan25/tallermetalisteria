package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Fabrica;
import tallerMetalisteria.modelo.Proveedor;

public class FabricaDAO {
	
	public static void crearFabricaDB(Fabrica fabrica) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `fabrica` (`codigo`, `nombre`, `direccion`, `Ciudad_id`) VALUES (?, ?, ?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, fabrica.getCodigo());
				ps.setString(2, fabrica.getNombre());
				ps.setString(3, fabrica.getDireccion());
				ps.setInt(4, fabrica.getCiudad_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego la Fabrica " + fabrica.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear la Fabrica");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<Fabrica> listarFabricasDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Fabrica> fabricas = new ArrayList<Fabrica>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `fabrica`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				fabricas.add(new Fabrica(rs.getString("codigo"), rs.getString("nombre"), rs.getString("direccion"), rs.getInt("Ciudad_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar las Fabricas");
			System.out.println(e);
		}
		
		return fabricas;
	}
	
	public static void actualizarFabricaDB(Fabrica fabrica) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE fabrica SET nombre = ?, direccion = ?, Ciudad_id = ? WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, fabrica.getNombre());
				ps.setString(2, fabrica.getDireccion());
				ps.setInt(3, fabrica.getCiudad_id());
				ps.setString(4, fabrica.getCodigo());
				ps.executeUpdate();
				
				System.out.println("La Fabrica " + fabrica.getNombre() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar la Fabrica");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarFabricaDB(String codigoFabrica) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM fabrica WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, codigoFabrica);
				ps.executeUpdate();
				
				System.out.println("La Fabrica con ID " + codigoFabrica +" ha sido eliminado");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la Fabrica");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
