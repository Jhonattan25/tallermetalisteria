package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Departamento;
import tallerMetalisteria.modelo.Proveedor;

public class ProveedorDAO {

	public void crearProveedorDB(Proveedor proveedor) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `proveedor` (`id`, `nombre`, `email`, `direccion`, `telefono`,`Ciudad_id`) VALUES (?, ?, ?, ?, ?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, proveedor.getId());
				ps.setString(2, proveedor.getNombre());
				ps.setString(3, proveedor.getEmail());
				ps.setString(4, proveedor.getDireccion());
				ps.setString(5, proveedor.getTelefono());
				ps.setInt(6, proveedor.getCiudad_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Proveedor " + proveedor.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Proveedor");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public ArrayList<Proveedor> listarProveedoresDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `proveedor`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				proveedores.add(new Proveedor(rs.getString("id"), rs.getString("nombre"), rs.getString("email"), rs.getString("direccion"), rs.getString("telefono"), rs.getInt("Ciudad_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Proveedores");
			System.out.println(e);
		}
		
		return proveedores;
	}
	
	public void actualizarProveedorDB(Proveedor proveedor) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE proveedor SET nombre = ?, email = ?, direccion = ?, telefono = ?, Ciudad_id = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, proveedor.getNombre());
				ps.setString(2, proveedor.getEmail());
				ps.setString(3, proveedor.getDireccion());
				ps.setString(4, proveedor.getTelefono());
				ps.setInt(5, proveedor.getCiudad_id());
				ps.setString(6, proveedor.getId());
				ps.executeUpdate();
				
				System.out.println("El Proveedor " + proveedor.getNombre() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el Proveedor");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public void eliminarProveedorDB(int idProveedor) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM proveedor WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idProveedor);
				ps.executeUpdate();
				
				System.out.println("El Proveedor con ID " + idProveedor +" ha sido eliminado");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar el Proveedor");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
