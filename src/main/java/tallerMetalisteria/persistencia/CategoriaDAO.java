package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Categoria;
import tallerMetalisteria.modelo.Ciudad;

public class CategoriaDAO {

	public static void crearCategoriaDB(Categoria categoria) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `categoria` (`nombre`, `descripcion`) VALUES (?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, categoria.getNombre());
				ps.setString(2, categoria.getDescripcion());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego la categoria " + categoria.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear la Categoria");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<Categoria> listarCategoriasDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Categoria> categorias = new ArrayList<Categoria>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `categoria`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				categorias.add(new Categoria(rs.getInt("id"), rs.getString("nombre"), rs.getString("descripcion")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar las categorias");
			System.out.println(e);
		}
		
		return categorias;
	}
	
	public static void actualizarCategoriaDB(Categoria categoria) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE categoria SET nombre = ?, descripcion = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, categoria.getNombre());
				ps.setString(2, categoria.getDescripcion());
				//ps.setInt(3, categoria.getId());
				ps.executeUpdate(); 
				
				System.out.println("La categoria " + categoria.getNombre() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar la categoria");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarCategoriaDB(int idCategoria) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM categoria WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idCategoria);
				ps.executeUpdate();
				
				System.out.println("La categoria con ID " + idCategoria +" ha sido eliminada");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la categoria");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
