package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.EPS;
import tallerMetalisteria.modelo.EPS;

public class EPSDAO {
	
	public static void crearEPSDB(EPS eps) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `eps` (`nombre`, `fechaIngreso`, `estado`)  VALUES (?, ?, ?) ";
				ps = conexion.prepareStatement(query);
				ps.setString(1, eps.getNombre());
				ps.setDate(2, eps.getFechaIngreso());
				ps.setBoolean(3, eps.isEstado());
				
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego la EPS " + eps.getNombre() + eps.getFechaIngreso() + eps.isEstado() + " con exito");

			} catch (SQLException ex) {
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<EPS> listarEPSDB() {
		Conexion db_connect = new Conexion();
		ArrayList<EPS> eps = new ArrayList<EPS>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `eps`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				eps.add(new EPS(rs.getInt("id"), rs.getString("nombre"), rs.getDate("fechaIngreso"), rs.getBoolean("estado")));
			}
			
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los EPS");
			System.out.println(e);
		}
		
		return eps;
	}
	
	public static void actualizarEPSDB(EPS eps) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE eps SET nombre = ?, fechaIngreso = ?, estado = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, eps.getNombre());
				ps.setDate(2, eps.getFechaIngreso());
				ps.setBoolean(3, eps.isEstado());
				ps.setInt(4, eps.getId());
				
				ps.executeUpdate();
				
				System.out.println("La EPS " + eps.getNombre() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar la EPS");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarEPSDB(int idEPS) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM eps WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idEPS);
				ps.executeUpdate();
				
				System.out.println("La EPS con ID " + idEPS +" ha sido eliminada");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la EPS");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
}
