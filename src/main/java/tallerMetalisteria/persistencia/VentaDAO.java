package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Local;
import tallerMetalisteria.modelo.Venta;

public class VentaDAO {
	public static void crearVentaDB(Venta venta) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `venta` (`codigo`,`fecha`, `iva`,`modoPago`,`total`,`descuento`,`Cliente_cedula`) VALUES (?, ?, ?, ?, ?, ?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, venta.getCodigo());
				ps.setDate(2, venta.getFecha());
				ps.setDouble(3, venta.getIva());
				ps.setString(4, venta.getModoPago());
				ps.setDouble(5, venta.getTotal());
				ps.setDouble(6, venta.getDescuento());
				ps.setInt(7, venta.getCedulaCliente());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println(query);
				System.out.println("Se agrego la venta " + venta.getCodigo() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear la venta");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<Venta> listarVentasDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Venta> ventas = new ArrayList<Venta>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `venta`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				ventas.add(new Venta(rs.getString("codigo"), rs.getDate("fecha"), rs.getInt("iva"), rs.getString("modoPago"), rs.getDouble("total"), rs.getDouble("descuento"), rs.getInt("Cliente_cedula")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los locales");
			System.out.println(e);
		}
		
		return ventas;
	}
	
	public static void actualizarVentaDB(Venta venta) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE venta SET fecha = ?, iva = ? , modoPago  = ?, total  = ?, descuento  = ?, Cliente_cedula  = ? WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				
				ps.setDate(1, venta.getFecha());
				ps.setDouble(2, venta.getIva());
				ps.setString(3,venta.getModoPago());
				ps.setDouble(4, venta.getTotal());
				ps.setDouble(5, venta.getDescuento());
				ps.setInt(6, venta.getCedulaCliente());
				ps.setString(7, venta.getCodigo());
				ps.executeUpdate();
				
				System.out.println("La venta" + venta.getCodigo() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el Local");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarVentaDB(String codigoVenta) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM venta WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, codigoVenta);
				ps.executeUpdate();
				
				System.out.println(" La Venta con ID " + codigoVenta +" ha sido eliminado");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la Venta");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
