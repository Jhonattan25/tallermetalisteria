package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Pais;

public class PaisDAO {

	public static void crearPaisDB(Pais pais) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `pais` (`nombre`) VALUES (?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, pais.getNombre());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Pa�s " + pais.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Pa�s");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<Pais> listarPaisesDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Pais> paises = new ArrayList<Pais>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `pais`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				paises.add(new Pais(rs.getInt("id"), rs.getString("nombre")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Paises");
			System.out.println(e);
		}
		
		return paises;
	}
	
	public static void actualizarPaisDB(Pais pais) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE pais SET nombre = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, pais.getNombre());
				ps.setInt(2, pais.getId());
				ps.executeUpdate();
				
				System.out.println("El Pa�s " + pais.getNombre() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el pa�s");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarPaisDB(int idPais) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM pais WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idPais);
				ps.executeUpdate();
				
				System.out.println("El Pais con ID " + idPais +" ha sido eliminado");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar el Pa�s");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
