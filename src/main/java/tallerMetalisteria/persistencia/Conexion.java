package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	private String url = "jdbc:mysql://localhost:3306/hrtallerymetalisteria?useTimezone=true&serverTimezone=UTC";
	private String usuario = "root";
	private String password = "sistemas";

	public Connection get_connection() {
		Connection conection = null;

		try {
			conection = DriverManager.getConnection(url, usuario, password);
			
			System.out.println("Todo full");
			
		} catch (SQLException e) {
			System.out.println(e);
		}

		return conection;
	}
}
