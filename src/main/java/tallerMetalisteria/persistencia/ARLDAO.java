package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.ARL;

public class ARLDAO {
	
	public static void crearARLDB(ARL arl) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement prepARL = null;
			try {
				String query = "INSERT INTO `arl` (`nombre`, `fechaIngreso`, `estado`)  VALUES (?, ?, ?) ";
				prepARL = conexion.prepareStatement(query);
				prepARL.setString(1, arl.getNombre());
				prepARL.setDate(2, arl.getFechaIngreso());
				prepARL.setBoolean(3, arl.isEstado());
				
				prepARL.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego la ARL " + arl.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<ARL> listarARLDB() {
		Conexion db_connect = new Conexion();
		ArrayList<ARL> arls = new ArrayList<ARL>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `arl`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				arls.add(new ARL(rs.getInt("id"), rs.getString("nombre"), rs.getDate("fechaIngreso"), rs.getBoolean("estado")));
			}
			
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los ARLS");
			System.out.println(e);
		}
		
		return arls;
	}
	
	public static void actualizarARLDB(ARL arl) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement prepARL = null;
			
			try {
				String query = "UPDATE arl SET nombre = ?, fechaIngreso = ?, estado = ? WHERE id = ?";
				prepARL = conexion.prepareStatement(query);
				prepARL.setString(1, arl.getNombre());
				prepARL.setDate(2, arl.getFechaIngreso());
				prepARL.setBoolean(3, arl.isEstado());
				prepARL.setInt(4, arl.getId());
				
				prepARL.executeUpdate();
				
				System.out.println("La ARL " + arl.getNombre() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar la ARL");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarARLDB(int idARL) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM arl WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idARL);
				ps.executeUpdate();
				
				System.out.println("La ARL con ID " + idARL +" ha sido eliminada");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la ARL");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
}
