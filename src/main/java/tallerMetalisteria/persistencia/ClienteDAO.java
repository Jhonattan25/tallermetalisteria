package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Cliente;

public class ClienteDAO {

	public void crearClienteDB(Cliente cliente) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `cliente` (`cedula`, `nombre`, `email`, `telefono`, `direccion`, `genero`, `Ciudad_id`) VALUES (?,?,?,?,?,?,?)";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, cliente.getDocumento());
				ps.setString(2, cliente.getNombre());
				ps.setString(3, cliente.getEmail());
				ps.setString(4, cliente.getTelefono());
				ps.setString(5, cliente.getDireccion());
				ps.setString(6, cliente.getGenero());
				ps.setInt(7, cliente.getCiudad_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Cliente " + cliente.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Cliente");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	
	public ArrayList<Cliente> listarClienteDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `cliente`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				clientes.add(new Cliente(rs.getInt("cedula"), rs.getString("nombre"), rs.getString("email"), rs.getString("telefono"),
						rs.getString("direccion"), rs.getString("genero"), rs.getInt("Ciudad_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Paises");
			System.out.println(e);
		}

		return clientes;
	}


	public void actualizarClienteDB(Cliente cliente) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "UPDATE cliente SET nombre = ?,  email = ?, telefono = ?, direccion = ?, Ciudad_id = ? WHERE cedula = ?";
				
				ps = conexion.prepareStatement(query);
				
				ps.setString(1, cliente.getNombre());
				ps.setString(2, cliente.getEmail());
				ps.setString(3, cliente.getTelefono());
				ps.setString(4, cliente.getDireccion());
				ps.setInt(5, cliente.getCiudad_id());
				ps.setInt(6, cliente.getDocumento());
				ps.executeUpdate();
				
				System.out.println("se actualizo El Cliente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el cliente");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}
	}

	public void eliminarClienteDB(int idCliente) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "DELETE FROM cliente WHERE cedula = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idCliente);
				ps.executeUpdate();

				System.out.println("El cargo con ID " + idCliente+ " ha sido eliminado");
			} catch (SQLException ex) {
				System.out.println("No se pudo eliminar el Cliente");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}  
 
	}

}
