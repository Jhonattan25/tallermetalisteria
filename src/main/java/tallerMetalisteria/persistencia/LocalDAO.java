package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import tallerMetalisteria.modelo.Local;

public class LocalDAO {
	public static void crearLocalDB(Local local) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `local` (`codigo`,`nombre`, `direccion`,`Ciudad_id`) VALUES (?, ?, ?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, local.getCodigo());
				ps.setString(2, local.getNombre());
				ps.setString(3, local.getDireccion());
				ps.setInt(4, local.getCiudad_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el local " + local.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el local");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public static ArrayList<Local> listarLocalesDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Local> locales = new ArrayList<Local>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `local`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				locales.add(new Local(rs.getString("codigo"), rs.getString("nombre"), rs.getString("direccion"),
						rs.getInt("Ciudad_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los locales");
			System.out.println(e);
		}

		return locales;
	}

	public static void actualizarLocalDB(Local local) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "UPDATE local SET nombre = ?, direccion = ? , Ciudad_id  = ? WHERE codigo = ?";
				ps = conexion.prepareStatement(query);

				ps.setString(1, local.getNombre());
				ps.setString(2, local.getDireccion());
				ps.setInt(3, local.getCiudad_id());
				ps.setString(4, local.getCodigo());
				ps.executeUpdate();

				System.out.println("El local " + local.getNombre() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el Local");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}
	}

	public static void eliminarLocalDB(String codigoLocal) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "DELETE FROM local WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, codigoLocal);
				ps.executeUpdate();

				System.out.println("El local con ID " + codigoLocal + " ha sido eliminado");
			} catch (SQLException ex) {
				System.out.println("No se pudo eliminar el local");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}

	}

	public ArrayList<Local> numeroEmpleadosDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Local> locales = new ArrayList<Local>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT Local_codigo, COUNT(*) AS numEmpleados FROM Empleado WHERE Local_codigo IS NOT NULL GROUP BY Local_codigo";

			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				locales.add(new Local(rs.getString("Local_codigo"), rs.getInt("numEmpleados")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los locales");
			System.out.println(e);
		}

		return locales;
	}

	public ArrayList<Local> numeroVentasDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Local> locales = new ArrayList<Local>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT Local.codigo, Local.nombre, COUNT(*) AS numVentas FROM Local JOIN productolocal ON Local.codigo = productolocal.Local_codigo" + 
					" JOIN detalleventa ON productolocal.id = detalleventa.ProductoLocal_id JOIN Venta ON detalleventa.Venta_codigo = Venta.codigo" + 
					" GROUP BY Local_codigo;";

			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				locales.add(new Local(rs.getString("codigo"), rs.getString("nombre"), rs.getInt("numVentas")));
			}
			
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los locales");
			System.out.println(e);
		}
		return locales;
	}

}
