package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Local;
import tallerMetalisteria.modelo.Pedido;
import tallerMetalisteria.modelo.Producto;

public class ProductoDAO {

	public static void crearProductoDB(Producto producto) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `producto` (`codigo`, `nombre`, `descripcion`, `iva`, `precio`, `cantidad`, `imagen`, `Fabrica_codigo`, `Categoria_id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, producto.getCodigo());
				ps.setString(2, producto.getNombre());
				ps.setString(3, producto.getDescripcion());
				ps.setDouble(4, producto.getIva());
				ps.setDouble(5, producto.getPrecio());
				ps.setInt(6, producto.getCantidad());
				ps.setString(7, producto.getImagen());
				ps.setString(8, producto.getFabrica_codigo());
				ps.setInt(9, producto.getCategoria_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego el Producto " + producto.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear el Producto");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public static ArrayList<Producto> listarProductosDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Producto> productos = new ArrayList<Producto>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `producto`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				productos.add(new Producto(rs.getString("codigo"), rs.getString("nombre"), rs.getString("descripcion"),
						rs.getDouble("iva"), rs.getDouble("precio"), rs.getInt("cantidad"), rs.getString("imagen"),
						rs.getString("Fabrica_codigo"), rs.getInt("Categoria_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los Productos");
			System.out.println(e);
		}

		return productos;
	}

	public static void actualizarProductoDB(Producto producto) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "UPDATE producto SET nombre = ?, descripcion = ?, iva = ?, precio = ?, cantidad = ?, imagen = ?, Fabrica_codigo = ?, Categoria_id = ? WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, producto.getNombre());
				ps.setString(2, producto.getDescripcion());
				ps.setDouble(3, producto.getIva());
				ps.setDouble(4, producto.getPrecio());
				ps.setInt(5, producto.getCantidad());
				ps.setString(6, producto.getImagen());
				ps.setString(7, producto.getFabrica_codigo());
				ps.setInt(8, producto.getCategoria_id());
				ps.setString(9, producto.getCodigo());
				ps.executeUpdate();

				System.out.println("El Producto " + producto.getNombre() + " se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar el Producto");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}
	}

	public static void eliminarProductoDB(String codigoProducto) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;

			try {
				String query = "DELETE FROM producto WHERE codigo = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, codigoProducto);
				ps.executeUpdate();

				System.out.println("El Producto con ID " + codigoProducto + " ha sido eliminado");
			} catch (SQLException ex) {
				System.out.println("No se pudo eliminar el Producto");
				System.out.println(ex);
			}
		} catch (SQLException e) {

			System.out.println(e);
		}

	}

	public ArrayList<Producto> productosFechaDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Producto> productos = new ArrayList<Producto>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT producto.codigo, producto.nombre, producto.descripcion, producto.iva, producto.precio FROM producto" + 
					" JOIN productolocal ON producto.codigo = productolocal.Producto_codigo JOIN detalleventa ON productolocal.id = detalleventa.ProductoLocal_id" + 
					" JOIN venta ON detalleventa.Venta_codigo = Venta.codigo" + 
					" WHERE venta.fecha BETWEEN \"2020-12-01\" AND \"2021-02-01\";";

			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos

			while (rs.next()) {

				productos.add(new Producto(rs.getString("codigo"), rs.getString("nombre"), rs.getString("descripcion"),
						rs.getDouble("iva"), rs.getDouble("precio")));
			}

		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar los locales");
			System.out.println(e);
		}
		return productos;
	}
}
