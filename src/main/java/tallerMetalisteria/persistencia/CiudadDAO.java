package tallerMetalisteria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tallerMetalisteria.modelo.Ciudad;
import tallerMetalisteria.modelo.Departamento;

public class CiudadDAO {
	
	public static void crearCiudadDB(Ciudad ciudad) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			try {
				String query = "INSERT INTO `ciudad` (`nombre`, `Departamento_id`) VALUES (?, ?)";
				ps = conexion.prepareStatement(query);
				ps.setString(1, ciudad.getNombre());
				ps.setInt(2, ciudad.getDepartamento_id());
				ps.executeUpdate(); // Finalmente ejecuta la consulta
				System.out.println("Se agrego la Ciudad " + ciudad.getNombre() + " con exito");

			} catch (SQLException ex) {
				System.out.println("No se pudo crear la Ciudad");
				System.out.println(ex);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static ArrayList<Ciudad> listarCiudadesDB() {
		Conexion db_connect = new Conexion();
		ArrayList<Ciudad> ciudades = new ArrayList<Ciudad>();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM `ciudad`";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery(); // Diferente al executeUpdate porque no va actualizar nada en la base de datos
			
			while (rs.next()) {
				
				ciudades.add(new Ciudad(rs.getInt("id"), rs.getString("nombre"), rs.getInt("Departamento_id")));
			}
		} catch (SQLException e) {
			System.out.println("No se pudieron recuperar las ciudades");
			System.out.println(e);
		}
		
		return ciudades;
	}
	
	public static void actualizarCiudadDB(Ciudad ciudad) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE ciudad SET nombre = ?, Departamento_id = ? WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setString(1, ciudad.getNombre());
				ps.setInt(2, ciudad.getDepartamento_id());
				ps.setInt(3, ciudad.getId());
				ps.executeUpdate();
				
				System.out.println("La Ciudad " + ciudad.getNombre() +" se actualiz� correctamente");
			} catch (Exception ex) {
				System.out.println("No se pudo editar la Ciudad");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}
	}
	
	public static void eliminarCiudadDB(int idCiudad) {
		Conexion db_connect = new Conexion();

		try (Connection conexion = db_connect.get_connection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "DELETE FROM ciudad WHERE id = ?";
				ps = conexion.prepareStatement(query);
				ps.setInt(1, idCiudad);
				ps.executeUpdate();
				
				System.out.println("La Ciudad con ID " + idCiudad +" ha sido eliminada");
			} catch(SQLException ex) {
				System.out.println("No se pudo eliminar la Ciudad");
				System.out.println(ex);
			}
		} catch(SQLException e) {

			System.out.println(e);
		}

	}
}
