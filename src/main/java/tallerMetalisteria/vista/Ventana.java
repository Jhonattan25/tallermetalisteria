package tallerMetalisteria.vista;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Ventana extends Application
{
	@Override
	public void start(Stage primaryStage) throws Exception
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/iniciarSesion.fxml"));
		
		Parent p = loader.load();
		Scene s = new Scene(p);
		
		primaryStage.setTitle("Iniciar Sesion");
		primaryStage.setResizable(false);
		primaryStage.setScene(s);
		//primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.show();
		//Ocultar ventana luego de una accion
//		primaryStage.hide();
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
}
