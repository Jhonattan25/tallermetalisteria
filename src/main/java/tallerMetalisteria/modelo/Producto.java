package tallerMetalisteria.modelo;

public class Producto {
	
	private String codigo;
	private String nombre;
	private String descripcion;
	private double iva;
	private double precio;
	private int cantidad;
	private String imagen;
	private String fabrica_codigo;
	private int categoria_id;
	
	public Producto() {
		super();
	}

	public Producto(String codigo, String nombre, String descripcion, double iva, double precio) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.iva = iva;
		this.precio = precio;
	}



	public Producto(String codigo, String nombre, String descripcion, double iva, double precio, int cantidad,
			String imagen, String fabrica_codigo, int categoria_id) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.iva = iva;
		this.precio = precio;
		this.cantidad = cantidad;
		this.imagen = imagen;
		this.fabrica_codigo = fabrica_codigo;
		this.categoria_id = categoria_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getFabrica_codigo() {
		return fabrica_codigo;
	}

	public void setFabrica_codigo(String fabrica_codigo) {
		this.fabrica_codigo = fabrica_codigo;
	}

	public int getCategoria_id() {
		return categoria_id;
	}

	public void setCategoria_id(int categoria_id) {
		this.categoria_id = categoria_id;
	}

	@Override
	public String toString() {
	  return codigo + ";" + nombre + ";" + descripcion + ";" + iva + ";" + precio + ";";
	}
}
