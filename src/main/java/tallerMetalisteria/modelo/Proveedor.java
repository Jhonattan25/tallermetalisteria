package tallerMetalisteria.modelo;

import java.util.ArrayList;

public class Proveedor {
	private String id;
	private String nombre;
	private String email;
	private String direccion;
	private String telefono;
	private int ciudad_id;

	public Proveedor() {
		super();
	}

	public Proveedor(String id, String nombre, String email, String direccion, String telefono, int ciudad_id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.direccion = direccion;
		this.telefono = telefono;
		this.ciudad_id = ciudad_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCiudad_id() {
		return ciudad_id;
	}

	public void setCiudad_id(int ciudad_id) {
		this.ciudad_id = ciudad_id;
	}

	@Override
	public String toString() {
		return id + ";" + nombre + ";" + email + ";" + direccion + ";" + telefono + ";" + ciudad_id + ";";
	}
}
