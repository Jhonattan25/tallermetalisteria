package tallerMetalisteria.modelo;

import java.sql.Date;

public class ARL {

	private int id; 
	private String nombre;
	private Date fechaIngreso;
	private boolean estado;
	
	public ARL() {
	}
	
	public ARL(String nombre, Date fechaIngreso, boolean estado) {
		super();
		this.nombre = nombre;
		this.fechaIngreso = fechaIngreso;
		this.estado = estado;
	}

	public ARL(int id, String nombre, Date fechaIngreso, boolean estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fechaIngreso = fechaIngreso;
		this.estado = estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
