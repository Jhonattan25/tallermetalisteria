package tallerMetalisteria.modelo;

import java.sql.Date;

public class Empleado {
	
	private int cedula;
	private String nombreCompleto;
	private String telefono;
	private String estadoCivil;
	private Date fechaNacimiento;
	private String direccion;
	private Date fechaIngreso;
	private String fabrica_codigo;
	private String genero;
	private String contrasenia;
	private String local_codigo;
	private int ciudad_id;
	private int eps_Id; 
	private int arl_Id;
	private int fondoPensiones_id;
	
	public Empleado() {
		super();
	}

	public Empleado(int cedula, String nombreCompleto, String telefono, String estadoCivil, Date fechaNacimiento,
			String direccion, Date fechaIngreso, String fabrica_codigo, String genero, String contrasenia,
			String local_codigo, int ciudad_id, int eps_Id, int arl_Id, int fondoPensiones_id) {
		super();
		this.cedula = cedula;
		this.nombreCompleto = nombreCompleto;
		this.telefono = telefono;
		this.estadoCivil = estadoCivil;
		this.fechaNacimiento = fechaNacimiento;
		this.direccion = direccion;
		this.fechaIngreso = fechaIngreso;
		this.fabrica_codigo = fabrica_codigo;
		this.genero = genero;
		this.contrasenia = contrasenia;
		this.local_codigo = local_codigo;
		this.ciudad_id = ciudad_id;
		this.eps_Id = eps_Id;
		this.arl_Id = arl_Id;
		this.fondoPensiones_id = fondoPensiones_id;
	}

	public int getCedula() {
		return cedula;
	}

	public void setCedula(int cedula) {
		this.cedula = cedula;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getFabrica_codigo() {
		return fabrica_codigo;
	}

	public void setFabrica_codigo(String fabrica_codigo) {
		this.fabrica_codigo = fabrica_codigo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getLocal_codigo() {
		return local_codigo;
	}

	public void setLocal_codigo(String local_codigo) {
		this.local_codigo = local_codigo;
	}

	public int getCiudad_id() {
		return ciudad_id;
	}

	public void setCiudad_id(int ciudad_id) {
		this.ciudad_id = ciudad_id;
	}

	public int getEps_Id() {
		return eps_Id;
	}

	public void setEps_Id(int eps_Id) {
		this.eps_Id = eps_Id;
	}

	public int getArl_Id() {
		return arl_Id;
	}

	public void setArl_Id(int arl_Id) {
		this.arl_Id = arl_Id;
	}

	public int getFondoPensiones_id() {
		return fondoPensiones_id;
	}

	public void setFondoPensiones_id(int fondoPensiones_id) {
		this.fondoPensiones_id = fondoPensiones_id;
	}
}