package tallerMetalisteria.modelo;

import java.sql.Date;

public class FondoPensiones {

	int id; 
	String nombre;
	Date fechaIngreso;
	boolean estado;
	
	public FondoPensiones(int id, String nombre, Date fechaingreso, boolean estado) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.nombre = nombre;
		this.fechaIngreso = fechaingreso;
		this.estado = estado;
	}
	public FondoPensiones(String nombreFP, Date fechaIngreso, boolean estadoFP) {
		// TODO Auto-generated constructor stub
		this.nombre = nombreFP;
		this.fechaIngreso = fechaIngreso;
		this.estado = estadoFP;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	} 
	
	
}
