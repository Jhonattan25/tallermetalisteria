package tallerMetalisteria.modelo;

public class Ciudad {

	private int id;
	private String nombre;
	private int departamento_id;
	
	public Ciudad() {
	}

	public Ciudad(String nombre, int departamento_id) {
		super();
		this.nombre = nombre;
		this.departamento_id = departamento_id;
	}

	public Ciudad(int id, String nombre, int departamento_id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.departamento_id = departamento_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDepartamento_id() {
		return departamento_id;
	}

	public void setDepartamento_id(int departamento_id) {
		this.departamento_id = departamento_id;
	}

	@Override
	public String toString() {
		return nombre;
	}
	
	
}
