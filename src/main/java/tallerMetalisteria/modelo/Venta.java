package tallerMetalisteria.modelo;

import java.sql.Date;

public class Venta {
	private String codigo;
	Date fecha;
	private double iva;
	private String modoPago;
	private double total;
	private double descuento;
	private int cedulaCliente;
	public Venta() {
		super();
	}
	
	public Venta(String codigo, Date fecha, double iva, String modoPago, double total, double descuento,
			int cedulaCliente) {
		super();
		this.codigo = codigo;
		this.fecha = fecha;
		this.iva = iva;
		this.modoPago = modoPago;
		this.total = total;
		this.descuento = descuento;
		this.cedulaCliente = cedulaCliente;
	}

	public Venta(Date fecha, double iva, String modoPago, double total, double descuento, int cedulaCliente) {
		super();
		this.fecha = fecha;
		this.iva = iva;
		this.modoPago = modoPago;
		this.total = total;
		this.descuento = descuento;
		this.cedulaCliente = cedulaCliente;
	}

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public String getModoPago() {
		return modoPago;
	}
	public void setModoPago(String modoPago) {
		this.modoPago = modoPago;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public int getCedulaCliente() {
		return cedulaCliente;
	}
	public void setCedulaCliente(int cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}
	@Override
	public String toString() {
		return "Venta [codigo=" + codigo + ", fecha=" + fecha + ", iva=" + iva + ", modoPago=" + modoPago + ", total="
				+ total + ", descuento=" + descuento + ", cedulaCliente="
				+ cedulaCliente + "]";
	}

}
