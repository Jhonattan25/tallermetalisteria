package tallerMetalisteria.modelo;

import java.sql.Date;

public class PQR {

	private int id;
	private String tipo;
	private Date fecha;
	private String contenido;
	private int cliente_cedula;
	private int empleadoCargo_id;
	
	public PQR() {
		super();
	}

	public PQR(int id, String tipo, Date fecha, String contenido, int cliente_cedula, int empleadoCargo_id) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.fecha = fecha;
		this.contenido = contenido;
		this.cliente_cedula = cliente_cedula;
		this.empleadoCargo_id = empleadoCargo_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public int getCliente_cedula() {
		return cliente_cedula;
	}

	public void setCliente_cedula(int cliente_cedula) {
		this.cliente_cedula = cliente_cedula;
	}

	public int getEmpleadoCargo_id() {
		return empleadoCargo_id;
	}

	public void setEmpleadoCargo_id(int empleadoCargo_id) {
		this.empleadoCargo_id = empleadoCargo_id;
	}
}
