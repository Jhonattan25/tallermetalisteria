package tallerMetalisteria.modelo;

public class Cargo {

	private int id;
	private String nombre;
	private String horario;
	private double salario;
	private String descripcion;
	
	public Cargo() {
		super();
	}

	public Cargo(int id, String nombre, String horario, double salario, String descripcion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.horario = horario;
		this.salario = salario;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
