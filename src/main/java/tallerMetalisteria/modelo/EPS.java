package tallerMetalisteria.modelo;

import java.sql.Date;

public class EPS {

	int id; 
	String nombre;
	Date fechaIngreso;
	boolean estado;
	
	public EPS(String nombreEPS, Date fechaIngresoEPS, boolean estadoEPS) {
		// TODO Auto-generated constructor stub
		this.nombre = nombreEPS;
		this.fechaIngreso = fechaIngresoEPS;
		this.estado = estadoEPS;
	}
	public EPS(int id, String nombre, Date fechaIngreso, boolean estado) {
		this.id = id;
		this.nombre = nombre;
		this.fechaIngreso = fechaIngreso;
		this.estado = estado;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	} 
	
}
