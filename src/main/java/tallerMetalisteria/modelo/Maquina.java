package tallerMetalisteria.modelo;

public class Maquina {

	private String codigo;
	private String nombre;
	private String modelo;
	private String marca;
	private String descripcion;
	private String fabrica_codigo;
	
	public Maquina() {
		super();
	}

	public Maquina(String codigo, String nombre, String modelo, String marca, String descripcion,
			String fabrica_codigo) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.modelo = modelo;
		this.marca = marca;
		this.descripcion = descripcion;
		this.fabrica_codigo = fabrica_codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFabrica_codigo() {
		return fabrica_codigo;
	}

	public void setFabrica_codigo(String fabrica_codigo) {
		this.fabrica_codigo = fabrica_codigo;
	}
}
