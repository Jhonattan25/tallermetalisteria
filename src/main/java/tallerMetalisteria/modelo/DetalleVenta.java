package tallerMetalisteria.modelo;

public class DetalleVenta {

	private int id;
	private String venta_codigo;
	private int productoLocal_id;
	private int cantidad;
	private double precio;
	
	public DetalleVenta() {
		super();
	}

	public DetalleVenta(int id, String venta_codigo, int productoLocal_id, int cantidad, double precio) {
		super();
		this.id = id;
		this.venta_codigo = venta_codigo;
		this.productoLocal_id = productoLocal_id;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVenta_codigo() {
		return venta_codigo;
	}

	public void setVenta_codigo(String venta_codigo) {
		this.venta_codigo = venta_codigo;
	}

	public int getProductoLocal_id() {
		return productoLocal_id;
	}

	public void setProductoLocal_id(int productoLocal_id) {
		this.productoLocal_id = productoLocal_id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
}
