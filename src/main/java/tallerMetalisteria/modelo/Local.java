package tallerMetalisteria.modelo;

public class Local {
	private String codigo;
	private String nombre;
	private String direccion;
	private int ciudad_id;
	private int numEmpleados;
	private int numVentas;
	
	public Local() {
		super();
	}
	
	public Local(String codigo, int numEmpleados) {
		super();
		this.codigo = codigo;
		this.numEmpleados = numEmpleados;
	}
	
	public Local(String codigo, String nombre, int numVentas) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.numVentas = numVentas;
	}
	
//	public Local(String nombre, String direccion, int ciudad_id) {
//		super();
//		this.nombre = nombre;
//		this.direccion = direccion;
//		this.ciudad_id = ciudad_id;
//	}

	public Local(String codigo, String nombre, String direccion, int ciudad_id) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad_id = ciudad_id;
	}

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getCiudad_id() {
		return ciudad_id;
	}
	public void setCiudad_id(int ciudad_id) {
		this.ciudad_id = ciudad_id;
	}
	
	public int getNumEmpleados() {
		return numEmpleados;
	}

	public void setNumEmpleados(int numEmpleados) {
		this.numEmpleados = numEmpleados;
	}
	
	public int getNumVentas() {
		return numVentas;
	}

	public void setNumVentas(int numVentas) {
		this.numVentas = numVentas;
	}

	@Override
	public String toString() {
		return codigo + ";" + numEmpleados +";";
	}
	
	public String toStringVentas() {
		return codigo + ";" + nombre + ";" + numVentas + ";";
	}
}
