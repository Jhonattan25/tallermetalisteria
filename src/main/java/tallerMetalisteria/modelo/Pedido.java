package tallerMetalisteria.modelo;

import java.sql.Date;

public class Pedido {
	
	private String codigo;
	private double precioTotal;
	private int cantidadUnidades;
	private Date fechaSolicitud;
	private double descuento;
	private String proveedor_id;
	private String fabrica_codigo;
	
	public Pedido() {
		super();
	}

	public Pedido(String codigo, double precioTotal, int cantidadUnidades, Date fechaSolicitud, double descuento,
			String proveedor_id, String fabrica_codigo) {
		super();
		this.codigo = codigo;
		this.precioTotal = precioTotal;
		this.cantidadUnidades = cantidadUnidades;
		this.fechaSolicitud = fechaSolicitud;
		this.descuento = descuento;
		this.proveedor_id = proveedor_id;
		this.fabrica_codigo = fabrica_codigo;
	}



	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}

	public int getCantidadUnidades() {
		return cantidadUnidades;
	}

	public void setCantidadUnidades(int cantidadUnidades) {
		this.cantidadUnidades = cantidadUnidades;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public String getProveedor_id() {
		return proveedor_id;
	}

	public void setProveedor_id(String proveedor_id) {
		this.proveedor_id = proveedor_id;
	}

	public String getFabrica_codigo() {
		return fabrica_codigo;
	}

	public void setFabrica_codigo(String fabrica_codigo) {
		this.fabrica_codigo = fabrica_codigo;
	}
	
}
