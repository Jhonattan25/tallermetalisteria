package tallerMetalisteria.modelo;

public class Fabrica {
	private String codigo;
	private String nombre;
	private String direccion;
	private int ciudad_id;
	
	public Fabrica() {
		super();
	}

	public Fabrica(String codigo, String nombre, String direccion, int ciudad_id) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad_id = ciudad_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getCiudad_id() {
		return ciudad_id;
	}

	public void setCiudad_id(int ciudad_id) {
		this.ciudad_id = ciudad_id;
	}
}
