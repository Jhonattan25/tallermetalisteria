package tallerMetalisteria.modelo;

public class Cliente {
	
	private int documento;
	private String nombre;
	private String email;
	private String telefono;
	private String direccion;
	private String genero;
	private int ciudad_id;
	
	
	public Cliente(int documento, String nombre, String email, String telefono, String direccion, String genero,
			int ciudad_id) {
		super();
		this.documento = documento;
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
		this.direccion = direccion;
		this.genero = genero;
		this.ciudad_id = ciudad_id;
	}
	
	public Cliente(int documento2, String telefono2, String nombre2, String email2, String direccion2, int ciudadId) {
		// TODO Auto-generated constructor stub
		this.documento = documento2;
		this.telefono= telefono2;
		this.email = email2;
		this.direccion = direccion2;
		this.ciudad_id = ciudadId;
	}

	public Cliente() {}

	public int getDocumento() {
		return documento;
	}
	public void setDocumento(int documento) {
		this.documento = documento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getCiudad_id() {
		return ciudad_id;
	}
	public void setCiudad_id(int ciudad_id) {
		this.ciudad_id = ciudad_id;
	}
	@Override
	public String toString() {
		return documento + ";" + nombre + ";" + email + ";" + telefono + ";"+ direccion + ";" + genero + ";" + ciudad_id + ";";
	}
}
