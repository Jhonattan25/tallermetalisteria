package tallerMetalisteria.modelo;

public class EmpleadoCargo {

	private int id;
	private int empleado_cedula;
	private int cargo_id;
	private String maquina_codigo;
	
	public EmpleadoCargo() {
		super();
	}

	public EmpleadoCargo(int id, int empleado_cedula, int cargo_id, String maquina_codigo) {
		super();
		this.id = id;
		this.empleado_cedula = empleado_cedula;
		this.cargo_id = cargo_id;
		this.maquina_codigo = maquina_codigo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmpleado_cedula() {
		return empleado_cedula;
	}

	public void setEmpleado_cedula(int empleado_cedula) {
		this.empleado_cedula = empleado_cedula;
	}

	public int getCargo_id() {
		return cargo_id;
	}

	public void setCargo_id(int cargo_id) {
		this.cargo_id = cargo_id;
	}

	public String getMaquina_codigo() {
		return maquina_codigo;
	}

	public void setMaquina_codigo(String maquina_codigo) {
		this.maquina_codigo = maquina_codigo;
	}
}
