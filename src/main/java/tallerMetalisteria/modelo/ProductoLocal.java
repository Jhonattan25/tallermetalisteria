package tallerMetalisteria.modelo;

public class ProductoLocal {

	private int id;
	private double precio;
	private String producto_codigo;
	private String local_codigo;
	
	public ProductoLocal() {
		super();
	}

	public ProductoLocal(int id, double precio, String producto_codigo, String local_codigo) {
		super();
		this.id = id;
		this.precio = precio;
		this.producto_codigo = producto_codigo;
		this.local_codigo = local_codigo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getProducto_codigo() {
		return producto_codigo;
	}

	public void setProducto_codigo(String producto_codigo) {
		this.producto_codigo = producto_codigo;
	}

	public String getLocal_codigo() {
		return local_codigo;
	}

	public void setLocal_codigo(String local_codigo) {
		this.local_codigo = local_codigo;
	}
}
